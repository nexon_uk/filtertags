#TagFilter#

A simple filter utility which will filter text according to the tags applied. Applications may include provided data or scripting for different environments, or generating different data sets. It is very simple and works like this-:

##Tag Parameters##
TagFilter parses text files. User passes a tag parameter to apply to TagFilter. TagFilter then filters the text, outputting only the text which is labelled with the tag parameter.  Inside the text file the tag lines look like this: `[-tagName-]`.  All text which follows on the lines below the tag "tagName" apply to the tag, until the file ends or another tag supercedes it.

##Wildcard Tag##
There is a wildcard tag [-all-]. Text belonging to this tag is always output.

##Variables##
TagFilter can parse variables. Variable names are preceded with '$'. e.g.
	`$MODE=test`

##Include files##
TagFilter will parse "#include" files as part of the text file being read.  e.g. `#include "master.config"`  will read the text from the file "master.config" before continuing parsing of the file being read.


